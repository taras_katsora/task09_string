package com.epam.model;

import com.epam.entity.Sentence;
import com.epam.entity.Word;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Domain {

    public static final List<Character> vowels =
            new ArrayList<>(
                    Arrays.asList('a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U'));
    private String text;
    private List<Sentence> sentences;

    public boolean readFile(String path) {
        StringBuilder builder = new StringBuilder();
        try (BufferedReader fileReader = new BufferedReader(
                new FileReader(path))) {
            String str;
            while ((str = fileReader.readLine()) != null) {
                builder.append(str);
                builder.append("\n");
            }
            builder.delete(builder.length() - 1, builder.length());
        } catch (FileNotFoundException e) {
            return false;
        } catch (IOException e) {
            throw new RuntimeException();
        }
        this.text = builder.toString()
                .replaceAll("\t+", " ")
                .replaceAll(" {2,}", " ");
        return true;
    }

    public Map<Word, Integer> findLargestNumberOfSentencesThatHaveSameWords() {
        Word[] uniqueWordsInText = getSentences().stream()
                .flatMap(s -> s.getWords().stream())
                .distinct()
                .toArray(Word[]::new);
        Map<Word, Integer> wordsMap = new HashMap<>();
        for (Word word : uniqueWordsInText) {
            for (Sentence sentence : getSentences()) {
                if (sentence.getWords().contains(word)) {
                    wordsMap.merge(word, 1, (a, b) -> a + b);
                    continue;
                }
            }
        }
        return getMapOfEntryWithMaxValue(wordsMap);
    }

    public List<Sentence> getSentencesInAscendingOrder() {
        return getSentences().stream()
                .sorted((s1, s2) -> s1.getWords().size() - s2.getWords().size())
                .collect(Collectors.toList());
    }

    public Word getUniqueWordOfFirstSentence() {
        label:
        for (Word word : getSentences().get(0).getWords()) {
            int counter = 0;
            for (int i = 1; i < getSentences().size(); i++) {
                if (getSentences().get(i).getWords().contains(word)) {
                    continue label;
                } else {
                    counter++;
                }
            }
            if (counter == getSentences().size() - 1) {
                return word;
            }
        }
        return new Word("");
    }

    public List<Word> getWordsOfQuestionSentences(int length) {
        List<Word> resultWords = getQuestionSentences()
                .stream()
                .flatMap(s -> s.getWords().stream())
                .filter(w -> w.getContents().length() == length)
                .distinct()
                .collect(Collectors.toList());
        return resultWords;
    }

    public List<String> replaceFirsWordWithVowelLetterToLongerWord() {
        List<String> resultSentencesList = new ArrayList<>();
        for (Sentence sentence : getSentences()) {
            Word word = getFirstWordBeginsWithVowelLetter(sentence);
            if (word != null) {
                String strSentence = sentence.getContents();
                String str = sentence.getContents();
                Pattern p = Pattern.compile("\\b" + word.getContents());
                Matcher m = p.matcher(str);
                if (m.find()) {
                    int start = m.start();
                    int end = m.end();
                    String resSentence = strSentence.substring(0, start);
                    String longestWord = getLongestWord(sentence).getContents();
                    resSentence += longestWord + strSentence.substring(end);
                    resultSentencesList.add(resSentence);
                } else {
                    resultSentencesList.add(strSentence);
                }
            }
        }
        return resultSentencesList;
    }

    public List<Sentence> sortWordsOfSentencesByFirstCharAndGetIt() {
        List<Sentence> sentencesList = new ArrayList<>(getSentences());
        for (Sentence sentence : sentencesList) {
            sentence.getWords()
                    .sort((w1, w2) ->
                            w1.getContents().charAt(0) - w2.getContents().charAt(0));
        }
        return sentencesList;
    }

    public List<Word> sortWordsOfTextByVowelsPerCent() {
        Map<Word, Double> wordsMap = new HashMap<>();
        for (Word word : getWordsOfAllSentences()) {
            wordsMap.put(
                    word, (double) getVowelsCount(word) / word.getContents().length());
        }
        return wordsMap.entrySet().stream()
                .sorted(Comparator.comparing(Map.Entry::getValue))
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }

    public List<Word> sortWordsBeginsWithVowelsByFirstConsonants() {
        return getSentences().stream()
                .flatMap(s -> s.getWords().stream())
                .distinct()
                .filter(w -> vowels.contains(w.getContents().charAt(0)))
                .sorted(Comparator.comparing(Word::getFirstConsonant))
                .collect(Collectors.toList());
    }

    public List<Word> sortWordsByIncreasingOfSomeChar(char c) {
        return getWordAndCountOfSomeCharOccurrence(c).entrySet().stream()
                .sorted(Map.Entry.<Word, Integer>comparingByValue()
                        .thenComparing(Map.Entry.comparingByKey()))
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }

    public List<Word> sortInputWordsByOccurrenceCountInTextDesc(
            List<Word> wordList) {
        Map<Word, Integer> map = new HashMap<>();
        for (Word word : wordList) {
            int count = (int) getSentences().stream()
                    .flatMap(s -> s.getWords().stream())
                    .filter(w -> w.equals(word))
                    .count();
            map.put(word, count);
        }
        return map.entrySet().stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }

    public List<Sentence> deleteSubstringThatBeginsAndEndsWithSomeChars(
            char startSymbol, char endSymbol) {
        List<Sentence> resultSentences = new ArrayList<>();
        for (Sentence sent : getSentences()) {
            Pattern p = Pattern.compile(startSymbol + ".*" + endSymbol);
            Matcher m = p.matcher(sent.getContents());
            String resultStr = sent.getContents();
            if (m.find()) {
                int begin = m.start();
                int end = m.end();
                resultStr = resultStr.substring(0, begin) + resultStr.substring(end);
            }
            resultSentences.add(
                    new Sentence(resultStr, getWordsOfSentence(resultStr)));
        }
        return resultSentences;
    }

    public String deleteWordsThatBeginsWithConsonantsCharAndHaveSomeLength(
            int length) {
        String resultText = text;
        List<Word> removeWords = getSentences().stream()
                .flatMap(s -> s.getWords().stream())
                .distinct()
                .filter(w ->
                        !vowels.contains(w.getContents().charAt(0))
                                && w.getContents().length() == length)
                .collect(Collectors.toList());
        for (Word word : removeWords) {
            resultText = resultText.replace(word.getContents(), "");
        }
        return resultText.replaceAll(" {2,}", " ").trim();
    }

    public List<Word> sortWordsByDecreasingOfSomeChar(char c) {
        return getWordAndCountOfSomeCharOccurrence(c).entrySet().stream()
                .sorted(Map.Entry.<Word, Integer>comparingByValue()
                        .reversed()
                        .thenComparing(Map.Entry.comparingByKey()))
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }

    public void findPalindrome() {
    }

    public String changeEveryWordByDeleteNextOccurrenceOfFirstChar() {
        String resultText = text;
        for (Word w : getWordsOfAllSentences()) {
            char c = w.getContents().charAt(0);
            StringBuilder builder = new StringBuilder();
            builder.append(c);
            char[] chars = w.getContents().toCharArray();
            for (int i = 1; i < w.getContents().length(); i++) {
                if (chars[i] != c) {
                    builder.append(chars[i]);
                }
            }
            resultText = resultText.replace(w.getContents(), builder.toString());
        }
        return resultText;
    }

    public Sentence replaceWordsWithSomeLengthBySomeString(
            int length, String str) {
        Random random = new Random();
        List<Sentence> sentenceList = getSentences();
        Sentence sentence = sentenceList
                .get(random.nextInt(sentenceList.size()));
        String resultSentence = sentence.getContents();
        for (Word w : sentence.getWords()) {
            if (w.getContents().length() == length) {
                resultSentence = resultSentence.replace(w.getContents(), str);
            }
        }
        return new Sentence(resultSentence, getWordsOfSentence(resultSentence));
    }


    private List<Sentence> getSentences() {
        if (sentences == null) {
            sentences = new ArrayList<>();
            String[] strArray = text.split("[.?!]");
            for (String str : strArray) {
                String sentence = str.trim();
                sentences.add(new Sentence(sentence, getWordsOfSentence(sentence)));
            }
        }
        return sentences;
    }

    private List<Sentence> getQuestionSentences() {
        List<Sentence> questionSent = new ArrayList<>();
        Pattern p = Pattern.compile("[A-Z][^.!?]+\\?");
        Matcher m = p.matcher(text);
        while (m.find()) {
            String sentence = m.group();
            List<Word> words = getWordsOfSentence(sentence);
            questionSent.add(new Sentence(sentence, words));
        }
        return questionSent;
    }

    private List<Word> getWordsOfSentence(String sentence) {
        String[] strWords = sentence.split("[^-’'\\w]+");
        return Arrays.stream(strWords)
                .filter(w -> !w.equals("-"))
                .map(Word::new)
                .collect(Collectors.toList());
    }

    private List<Word> getWordsOfAllSentences() {
        return getSentences().stream()
                .flatMap(s -> s.getWords().stream())
                .distinct()
                .collect(Collectors.toList());
    }

    private Map<Word, Integer> getWordAndCountOfSomeCharOccurrence(char c) {
        Map<Word, Integer> wordsMap = new HashMap<>();
        for (Word word : getWordsOfAllSentences()) {
            wordsMap.put(
                    word, getSomeCharCount(word, c));
        }
        return wordsMap;
    }

    private Word getLongestWord(Sentence sentence) {
        return sentence.getWords().stream()
                .max((w1, w2) -> w1.getContents().length() - w2.getContents().length())
                .get();
    }

    private Word getFirstWordBeginsWithVowelLetter(Sentence sentence) {
        for (Word word : sentence.getWords()) {
            if (word.getContents().matches("\\b[aeiouAEIOU](\\w+)?")) {
                return word;
            }
        }
        return null;
    }

    private Map<Word, Integer> getMapOfEntryWithMaxValue(Map<Word, Integer> map) {
        Optional<Entry<Word, Integer>> maxEntry = map.entrySet()
                .stream()
                .max(Comparator.comparing(Entry::getValue));
        Map<Word, Integer> result = new HashMap<>();
        result.put(maxEntry.get().getKey(), maxEntry.get().getValue());
        return result;
    }

    private int getVowelsCount(Word word) {
        char[] chars = word.getContents().toCharArray();
        int vowelCount = 0;
        for (char c : chars) {
            if (vowels.contains(c)) {
                vowelCount++;
            }
        }
        return vowelCount;
    }

    private int getSomeCharCount(Word word, char searchChar) {
        char[] chars = word.getContents().toCharArray();
        int charCount = 0;
        for (char c : chars) {
            if (Character.toLowerCase(c) == searchChar
                    || Character.toUpperCase(c) == searchChar) {
                charCount++;
            }
        }
        return charCount;
    }
}
