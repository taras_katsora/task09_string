package com.epam.entity;

import com.epam.model.Domain;

import java.util.List;

public class Sentence {

    private String contents;
    private List<Word> words;

    public Sentence(String contents,
                    List<Word> words) {
        this.contents = contents;
        this.words = words;
    }

    public String getContents() {
        return contents;
    }

    public List<Word> getWords() {
        return words;
    }

    @Override
    public String toString() {
        return contents;
    }
}