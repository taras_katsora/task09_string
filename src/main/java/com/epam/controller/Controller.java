package com.epam.controller;

import com.epam.entity.Word;
import com.epam.entity.Sentence;
import com.epam.model.Text;

import java.util.List;
import java.util.Map;

public class Controller {
    private Text model;

    public Controller(Text model) {
        this.model = model;
    }

    public boolean readFile(String path) {
        return model.readFile(path);
    }

    public Map<Word, Integer> findLargestNumberOfSentencesThatHaveSameWords() {
        return model.findLargestNumberOfSentencesThatHaveSameWords();
    }

    public List<Sentence> getSentencesInAscendingOrder() {
        return model.getSentencesInAscendingOrder();
    }

    public Word getUniqueWordOfFirstSentence() {
        return model.getUniqueWordOfFirstSentence();
    }

    public List<Word> getWordsOfQuestionSentences(int length) {
        return model.getWordsOfQuestionSentences(length);
    }

    public List<String> replaceFirsWordWithVowelLetterToLongerWord() {
        return model.replaceFirsWordWithVowelLetterToLongerWord();
    }

    public List<Sentence> sortWordsOfSentencesByFirstCharAndGetIt() {
        return model.sortWordsOfSentencesByFirstCharAndGetIt();
    }

    public List<Word> sortWordsOfTextByVowelsPerCent() {
        return model.sortWordsOfTextByVowelsPerCent();
    }

    public List<Word> sortWordsBeginsWithVowelsByFirstConsonants() {
        return model.sortWordsBeginsWithVowelsByFirstConsonants();
    }

    public List<Word> sortWordsByIncreasingOfSomeChar(char c) {
        return model.sortWordsByIncreasingOfSomeChar(c);
    }

    public List<Word> sortInputWordsByOccurrenceCountInTextDesc(
            List<Word> words) {
        return model.sortInputWordsByOccurrenceCountInTextDesc(words);
    }

    public List<Sentence> deleteSubstringThatBeginsAndEndsWithSomeChars(
            char startSymbol, char endSymbol) {
        return model
                .deleteSubstringThatBeginsAndEndsWithSomeChars(startSymbol,endSymbol);
    }

    public String deleteWordsThatBeginsWithConsonantsCharAndHaveSomeLength(
            int length) {
        return model
                .deleteWordsThatBeginsWithConsonantsCharAndHaveSomeLength(length);
    }

    public List<Word> sortWordsByDecreasingOfSomeChar(char c) {
        return model.sortWordsByDecreasingOfSomeChar(c);
    }

    public String changeEveryWordByDeleteNextOccurrenceOfFirstChar() {
        return model.changeEveryWordByDeleteNextOccurrenceOfFirstChar();
    }

    public Sentence replaceWordsWithSomeLengthBySomeString(
            int length, String str) {
        return model.replaceWordsWithSomeLengthBySomeString(length, str);
    }
}