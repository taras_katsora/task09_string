package com.epam.view;

import com.epam.entity.Sentence;
import com.epam.controller.Controller;
import com.epam.entity.Word;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class MyView {

    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static BufferedReader reader =
            new BufferedReader(new InputStreamReader(System.in));

    public MyView(Controller controller) {
        this.controller = controller;
        menu = new LinkedHashMap<>();
        menu.put("1", "1 - Знайти найбільшу кількість речень тексту, в яких є "
                + "однакові слова.");
        menu.put("2", "2 - Вивести всі речення заданого тексту у порядку зростання"
                + " кількості слів у кожному з них.");
        menu.put("3", "3 - Знайти таке слово у першому реченні, якого немає "
                + "ні в одному з інших речень");
        menu.put("4", "4 - У всіх запитальних реченнях тексту знайти і"
                + " надрукувати без повторів слова заданої довжини.");
        menu.put("5", "5 - У кожному реченні тексту поміняти місцями перше слово,"
                + " що починається на голосну букву з найдовшим словом");
        menu.put("6", "6 - Надрукувати слова тексту в алфавітному порядку "
                + "по першій букві. Слова,що починаються з нової букви, друкувати"
                + " з абзацного відступу.");
        menu.put("7", "7 - Відсортувати слова тексту за зростанням відсотку "
                + "голосних букв.");
        menu.put("8",
                "8 - Слова тексту, що починаються з голосних букв, відсортувати"
                        + " в алфавітному порядку по першій приголосній букві слова");
        menu.put("9", "9 - Всі слова тексту відсортувати за зростанням кількості "
                + "заданої букви у слові. Слова з однаковою кількістю розмістити "
                + "у алфавітному порядку.");
        menu.put("10", "10 - Для кожного слова з заданого списку знайти, "
                + "скільки разів воно зустрічається у кожному реченні, "
                + "і відсортувати слова за спаданням загальної кількості входжень");
        menu.put("11",
                "11 - У кожному реченні тексту видалити підрядок максимальної"
                        + " довжини, що починається і закінчується заданими символами");
        menu.put("12", "12 - З тексту видалити всі слова заданої довжини, "
                + "що починаються на приголосну букву.");
        menu.put("13", "13 - Відсортувати слова у тексті за спаданням кількості "
                + "входжень заданого символу, а у випадку рівності – за алфавітом.");
        menu.put("15", "15 - Перетворити кожне слово у тексті, видаливши з нього "
                + "всі наступні входження першої букви цього слова.");
        menu.put("16", "16 - У певному реченні тексту слова заданої довжини "
                + "замінити вказаним підрядком, довжина якого може не співпадати"
                + " з довжиною слова.");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu
                .put("1", this::findLargestNumberOfSentencesThatHaveSameWords);
        methodsMenu
                .put("2", this::printSentencesInAscendingOrder);
        methodsMenu
                .put("3", this::printUniqueWordOfFirstSentence);
        methodsMenu
                .put("4", this::printWordsOfQuestionSentences);
        methodsMenu
                .put("5", this::replaceFirsWordWithVowelLetterToLongerWord);
        methodsMenu
                .put("6", this::printSortedWordsOfSentences);
        methodsMenu
                .put("7", this::printSortedWordsOfTextByVowelsPerCent);
        methodsMenu
                .put("8", this::printSortWordsBeginsWithVowelsByFirstConsonants);
        methodsMenu
                .put("9", this::printSortedWordsByIncreasingOfSomeChar);
        methodsMenu
                .put("10", this::printInputWordsSortedInDescByOccurrenceCountInText);
        methodsMenu
                .put("11", this::deleteSubstringThatBeginsAndEndsWithSomeChars);
        methodsMenu
                .put("12",
                        this::deleteWordsThatBeginsWithConsonantsCharAndHaveSomeLength);
        methodsMenu
                .put("13", this::printSortedWordsByDecreasingOfSomeChar);
        methodsMenu
                .put("15", this::changeEveryWordByDeleteNextOccurrenceOfFirstChar);
        methodsMenu
                .put("16", this::replaceWordsWithSomeLengthBySomeString);
    }

    public void start() throws IOException {
        System.out.println("Enter full path to text file:");
        boolean isRead = false;
        while (true) {
            String path = reader.readLine();
            if (path.equalsIgnoreCase("q")) {
                break;
            } else if (controller.readFile(path)) {
                isRead = true;
                System.out.println("File successfully read.");
                break;
            }
            System.out.println("File not found. Try again or enter \"Q\" - to exit:");
        }
        if (isRead) {
            show();
        }
    }

    private void printMenu() {
        System.out.println("MENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    private void show() throws IOException {
        String choice;
        do {
            System.out.println("-------------------------------------------");
            printMenu();
            System.out.println("-------------------------------------------");
            System.out.println("Please, select menu point.");
            System.out.println("-------------------------------------------");
            choice = reader.readLine();
            Printable  printable = methodsMenu.get(choice);
            if (printable != null) {
                printable.print();
            }
        } while (!choice.equalsIgnoreCase("Q"));
    }

    private void findLargestNumberOfSentencesThatHaveSameWords() {
        System.out.println("Word that contains max count of sentences:");
        System.out.println(controller
                .findLargestNumberOfSentencesThatHaveSameWords());
    }

    private void printSentencesInAscendingOrder() {
        System.out.println("Sentences in Ascending Order:");
        for (Sentence sentence : controller.getSentencesInAscendingOrder()) {
            System.out.println(sentence.getContents() + ".");
        }
    }

    private void printUniqueWordOfFirstSentence() {
        System.out.println(controller.getUniqueWordOfFirstSentence());
    }

    public void printWordsOfQuestionSentences() {
        System.out.println("Enter word length:");
        System.out.println(
                controller.getWordsOfQuestionSentences(getNumber()));
    }

    public void replaceFirsWordWithVowelLetterToLongerWord() {
        System.out.println("Result:");
        controller
                .replaceFirsWordWithVowelLetterToLongerWord()
                .forEach(s -> System.out.println(s + "."));
    }

    public void printSortedWordsOfSentences() {
        List<Sentence> sentences =
                controller.sortWordsOfSentencesByFirstCharAndGetIt();
        for (Sentence s : sentences) {
            List<Word> words = s.getWords();
            System.out.printf("%s", words.get(0).getContents());
            for (int i = 1; i < words.size(); i++) {
                if (words.get(i).getContents().charAt(0) !=
                        words.get(i - 1).getContents().charAt(0)) {
                    System.out.printf("\n%s", words.get(i).getContents());
                } else {
                    System.out.printf("\t%s", words.get(i).getContents());
                }
            }
            System.out.println("\n-------------------------------------------");
        }
    }

    public void printSortedWordsOfTextByVowelsPerCent() {
        controller.sortWordsOfTextByVowelsPerCent().forEach(System.out::println);
    }

    public void printSortWordsBeginsWithVowelsByFirstConsonants() {
        controller
                .sortWordsBeginsWithVowelsByFirstConsonants()
                .forEach(System.out::println);
    }

    public void printSortedWordsByIncreasingOfSomeChar() {
        List<Word> list = controller
                .sortWordsByIncreasingOfSomeChar(getCharacter());
        for (Word word : list) {
            System.out.println(word);
        }
    }

    public void printInputWordsSortedInDescByOccurrenceCountInText() {
        try {
            controller
                    .sortInputWordsByOccurrenceCountInTextDesc(getWords())
                    .forEach(System.out::println);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void deleteSubstringThatBeginsAndEndsWithSomeChars() {
        char start = getCharacter();
        char end = getCharacter();
        controller.deleteSubstringThatBeginsAndEndsWithSomeChars(start,end)
                .forEach(System.out::println);
    }

    public void deleteWordsThatBeginsWithConsonantsCharAndHaveSomeLength() {
        System.out.println("Enter length of word:");
        String result = controller
                .deleteWordsThatBeginsWithConsonantsCharAndHaveSomeLength(getNumber());
        System.out.println(result);
    }

    public void printSortedWordsByDecreasingOfSomeChar() {
        controller
                .sortWordsByDecreasingOfSomeChar(getCharacter())
                .forEach(System.out::println);
    }

    public void changeEveryWordByDeleteNextOccurrenceOfFirstChar() {
        String result = controller
                .changeEveryWordByDeleteNextOccurrenceOfFirstChar();
        System.out.println(result);
    }

    public void replaceWordsWithSomeLengthBySomeString() {
        System.out.println("Enter length:");
        int length = getNumber();
        String str = "";
        try {
            str = getString();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        System.out.println(
                controller.replaceWordsWithSomeLengthBySomeString(length, str));
    }

    private int getNumber() {
        int length = 0;
        boolean isCorrect = false;
        while (!isCorrect) {
            try {
                length = Integer.parseInt(reader.readLine());
                if (length <= 0) {
                    throw new NumberFormatException();
                }
                isCorrect = true;
            } catch (NumberFormatException e) {
                System.out.println("Incorrect length, try again:");
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        return length;
    }

    private char getCharacter() {
        System.out.println("Enter character:");
        char character = 0;
        try {
            character = reader.readLine().charAt(0);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return character;
    }

    private List<Word> getWords() throws IOException {
        System.out.println("Enter some words:");
        List<Word> words = new ArrayList<>();
        String str;
        while (!(str = reader.readLine()).isEmpty()) {
            words.add(new Word(str));
        }
        return words;
    }

    private String getString() throws IOException {
        System.out.println("Enter some string:");
        StringBuilder builder = new StringBuilder();
        String str;
        while (!(str = reader.readLine()).isEmpty()) {
            builder.append(str);
            builder.append("\n");
        }
        builder.delete(builder.length() - 1, builder.length());
        return builder.toString();
    }
}

