package com.epam;

import com.epam.model.Domain;
import com.epam.model.Text;
import com.epam.view.MyView;
import com.epam.controller.Controller;

import java.io.IOException;

public class Application {

    public static void main(String[] args) throws IOException {
        Domain domain = new Domain();
        Text model = new Text(domain);
        Controller controller = new Controller(model);
        MyView view = new MyView(controller);
        view.start();
    }
}
